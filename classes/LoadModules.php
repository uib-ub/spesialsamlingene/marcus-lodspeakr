<?php declare(strict_types=1);

namespace uib\ub\loadspeakr;

use uib\ub\loadspeakr\modules\ModuleInterface;
use uib\ub\loadspeakr\modules\UnknownModule;

final class LoadModules
{
    private Configuration $configuration;
    private Utils $utils;

    public function __construct(Configuration $configuration, Utils $utils)
    {
        $this->configuration = $configuration;
        $this->utils = $utils;
    }

  public function loadModule(string $uri): ModuleInterface
  {
      $className =  $this->loadClass($uri);
      $unknown = new UnknownModule();

      switch ($uri) {
          case 'admin':
              return new $className();
          case 'redirect':
              return new $className();
          case 'service':
              return new $className($this->configuration, $this->utils);
          case 'session':
              return new $className();
          case 'sparqlfilter':
              return new $className();
          case 'type':
              return new $className($this->configuration, $this->utils);
          case 'uri':
              return new $className($this->configuration, $this->utils);
          case 'export':
              return new $className();
          case 'static':
              return new $className($this->configuration);
          case 'default':
              break;
      }

      return $unknown;
  }

    private function loadClass($module): string
    {
        if (!$this->moduleAvailable($module)) {
            HTTPStatus::send500('Module: ' . $module . ' not found.');
        }

        $className = ucfirst($module) . 'Module';

        if (file_exists(__DIR__ . '/modules/' . $className . '.php')) {
            return __NAMESPACE__ . '\\modules\\' . $className;
        }

        return '';
    }

    private function moduleAvailable(string $module): bool
    {
        if (!in_array($module, $this->configuration->getConfigValue('conf', 'modules')['available'], true)) {
            return false;
        }

        return true;
    }

}