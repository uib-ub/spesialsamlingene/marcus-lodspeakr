<?php declare(strict_types=1);

namespace uib\ub\loadspeakr;


use UnexpectedValueException;

final class Endpoint
{
    private string $sparqlUrl;
    private array $params;

    public function __construct(string $sparqlUrl, array $params)
    {
        $this->sparqlUrl = $sparqlUrl;
        $this->params = $params;
    }

    public function query($q, $output = 'json')
    {
        global $conf;
        $accept = 'application/sparql-results+json';
        $modified = 0;
        $aux = '';

        if ($output === 'xml') {
            $accept = 'application/sparql-results+xml';
        } elseif ($output === 'rdf') {
            $accept = 'application/rdf+xml';
        }

        $cacheFile = $conf['home'] . "cache/query" . md5($this->sparqlUrl . $q);
        if ($this->caching($conf)) {
            if (file_exists($cacheFile)) {
                $modified = filemtime($cacheFile);
            }

            /**
             * If we have cached content lets use it.
             */
            if ($modified + $conf['cache']['global'] > time()) {
                $aux = file_get_contents($cacheFile);
            }
        }

        if (!$aux) {
            $result = $this->curl($q, $accept, $conf);
            $aux = $result['result'];
            $this->updateCache($result['status'], $cacheFile, $aux, $conf);
        }

        if (!$aux) {
            return $aux;
        }

        return $this->result($q, $aux);
    }

    private function result(string $q, string $aux)
    {
        if (false !== stripos($q, "select")) {
            return json_decode($aux, true);
        }

        if (false !== stripos($q, "describe")) {
            return $aux;
        }

        if (false !== stripos($q, "construct")) {
            return $aux;
        }

        if (false !== stripos($q, "ask")) {
            return json_decode($aux, true);
        }

        return $aux;
    }

    private function caching(array $conf): bool
    {
        return !empty($conf['cache']['global']) && is_int($conf['cache']['global']) && $conf['cache']['global'] > 0;
    }

    private function updateCache($http_status, $cacheFile, $aux, $conf): void
    {
        if ($http_status === 200 && $this->caching($conf)) {
            $result = file_put_contents($cacheFile, ($aux), LOCK_EX);

            if (!$result) {
                throw new UnexpectedValueException(
                  'Unable to update cache: ' . $cacheFile
                );
            }
        }
    }

    private function curl(string $request, $accept, array $conf): array
    {
        $resource = curl_init();
        $context = ['Connection: close', 'Accept: ' . $accept];
        $params = $this->params;
        $params['query'] = $request;
        $url = $this->sparqlUrl . '?' . http_build_query($params, '', '&');
        curl_setopt($resource, CURLOPT_URL, $url);
        curl_setopt($resource, CURLOPT_HTTPHEADER, $context);
        curl_setopt($resource, CURLOPT_USERAGENT, "LODSPeaKr version " . $conf['version']);
        curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($resource);
        $http_status = (int)curl_getinfo($resource, CURLINFO_HTTP_CODE);
        curl_close($resource);

        return [
            'result' => $result,
            'status' => $http_status,
        ];
    }

    public function queryPost(string $request)
    {
        $params = $this->params;
        $params['query'] = $request;
        $resource = curl_init();
        curl_setopt($resource, CURLOPT_URL, $this->sparqlUrl);
        curl_setopt($resource, CURLOPT_POST, count($params));
        curl_setopt($resource, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);

        return curl_exec($resource);
    }

    public function getSparqlURL(): string
    {
        return $this->sparqlUrl;
    }

}
