<?php declare(strict_types=1);

namespace uib\ub\loadspeakr;

use stdClass;

final class HTTPStatus
{
    public static function send303($uri, $ext): void
    {
        header("HTTP/1.0 303 See Other");
        header("Location: " . $uri);
        header("Content-type: " . $ext);
        echo self::getContent("303", $uri);
        exit(0);
    }

    public static function send401($uri): void
    {
        header("HTTP/1.0 401 Forbidden");
        echo self::getContent("401", $uri);
        exit(0);
    }

    public static function send404($uri): void
    {
        header("HTTP/1.0 404 Not Found");
        $alt = "LODSPeaKr couldn't find the resource " . $uri;
        echo self::getContent("404", $alt);
        exit(0);
    }

    public static function send406($uri): void
    {
        header("HTTP/1.0 406 Not Acceptable");
        $alt = "LODSPeaKr can't return content acceptable according to the Accept headers sent in the request for " . $uri;
        echo self::getContent("406", $alt);
        exit(0);
    }

    public static function send500($uri): void
    {
        header("HTTP/1.0 500 Internal Server Error");
        $alt = "There was an internal error when processing " . $uri;
        echo self::getContent("500", $alt);
        exit(0);
    }

    private static function getContent($n, $alt): string
    {
        global $conf;
        global $lodspk;
        $lodspk['root'] = $conf['root'];
        $lodspk['home'] = $conf['basedir'];
        $lodspk['baseUrl'] = $conf['basedir'];
        $lodspk['ns'] = $conf['ns'];
        $file = $conf['httpStatus']['directory'] . "/" . $n . ".template";

        if (file_exists($conf['home'] . $file)) {
            Utils::showView($lodspk, new stdClass(), $file, $conf['home']);
            return '';
        }

        return $alt . "\n\n";
    }
}
