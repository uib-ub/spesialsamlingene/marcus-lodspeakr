<?php declare(strict_types=1);

namespace uib\ub\loadspeakr\modules;

interface ModuleInterface
{
    public function match($uri);
    public function execute($params);
}
