<?php declare(strict_types=1);

namespace uib\ub\loadspeakr\modules;

use uib\ub\loadspeakr\MetaDb;

class RedirectModule implements ModuleInterface
{
    public function match($uri)
    {
        global $conf;
        global $localUri;
        global $uri;
        global $acceptContentType;
        global $endpoints;
        global $lodspk;

        $metaDb = new MetaDb($conf['metadata']['db']['location']);

        return true;
    }

    public function execute($pair)
    {
        global $conf;
        global $localUri;
        global $uri;
        global $acceptContentType;
        global $endpoints;
        global $lodspk;
        global $results;
        global $firstResults;

        echo $uri . " is The uri";
    }

}
