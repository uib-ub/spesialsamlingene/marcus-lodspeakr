<?php declare(strict_types=1);

namespace uib\ub\loadspeakr\modules;

use uib\ub\loadspeakr\HTTPStatus;

/**
 * Null result Loadspkr module.
 *
 * Used when unknown module is requested.
 */
final class UnknownModule implements ModuleInterface
{

    public function match($uri): void
    {
        HTTPStatus::send404($uri);
    }

    public function execute($params): void
    {
    }
}
