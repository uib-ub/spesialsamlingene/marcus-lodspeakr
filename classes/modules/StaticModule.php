<?php declare(strict_types=1);

namespace uib\ub\loadspeakr\modules;

use uib\ub\loadspeakr\Configuration;
use uib\ub\loadspeakr\Endpoint;
use uib\ub\loadspeakr\HTTPStatus;
use uib\ub\loadspeakr\Logging;
use uib\ub\loadspeakr\Utils;

/**
 * Serves static content, such as HTML pages, Javascript and CSS files.
 */
class StaticModule implements ModuleInterface
{
    private Configuration $configuration;
    private string $basedir;
    private Endpoint $endpoints;
    private string $localUri;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
        $this->basedir = $configuration->getConfigValue('conf', 'basedir');
        $this->endpoints = $configuration->getConfigValue('bootstrap', 'endpoints')['local'];
        $this->localUri = $configuration->getConfigValue('bootstrap', 'localUri');
    }

    public function match($uri)
    {
        $home = $this->configuration->getConfigValue('conf', 'home');
        $staticDirectory = $this->configuration->getConfigValue('conf', 'static')['directory'];
        $queryFile = preg_replace('|^' . $this->basedir . '|', '', $this->localUri);

        if ($queryFile !== NULL && file_exists($home . $staticDirectory . $queryFile)) {
            return $queryFile;
        }

        return false;
    }

    private function sendHeader(string $fileName): void
    {
        $filePaths = explode('.', $fileName);
        $extension = end($filePaths);
        header('Content-type: ' . $this->getContentType($extension));
    }

    private function isHtml($fileName): int
    {
        $htmlExtension = 'html';
        return substr_compare($fileName, $htmlExtension, -strlen($htmlExtension), strlen($htmlExtension));
    }

    private function processDocumentPrepare(): array
    {
        $acceptContentType = $this->configuration->getConfigValues('bootstrap')['acceptContentType'];
        $conf = $this->configuration->getConfigValues('conf');
        $lodspk = [
            'home' => $conf['basedir'],
            'baseUrl' => $conf['basedir'],
            'module' => 'static',
            'root' => $conf['root'],
            'contentType' => $acceptContentType,
            'ns' => $conf['ns'],
            'this' => [
                'value' => $this->localUri,
                'curie' => Utils::uri2curie($this->localUri),
            ],
            'local' => [
                'value' => $this->localUri,
                'curie' => Utils::uri2curie($this->localUri),
            ],
            'endpoint' => $conf['endpoint'],
        ];

        return array_merge(
          $lodspk,
          $this->configuration->getConfigValues('bootstrap')['lodspk']
        );
    }

    public function execute($params): void
    {
        /**
         * @todo Remove dangerous use and change of a global variable.
         */
        global $uri;
        $uri = $this->localUri;

        $staticDirectory = $this->configuration->getConfigValue('conf', 'static')['directory'];
        $this->validateDirectory($params);
        $this->sendHeader($params);

        if ($this->configuration->getConfigValue('conf', 'debug')) {
            Logging::log("In " . $staticDirectory . " static file $params");
        }

        if ($this->configuration->getConfigValue('conf', 'static')['haanga'] && $this->isHtml($params) === 0) {
            $lodspk = $this->processDocumentPrepare();
            Utils::processDocument($staticDirectory . $params, $lodspk, new \stdClass());
        } else {
            echo file_get_contents($staticDirectory . $params);
        }
    }

    /**
     * Based on file extension get correct content type MIME header.
     *
     * @param string $extension file extension.
     * @return string MIME header.
     */
    private function getContentType(string $extension): string
    {
        $contentTypes = [
          'html' => 'text/html',
          'css' => 'text/css',
          'js' => 'application/javascript',
          'json' => 'application/json',
          'jsonp' => 'application/javascript',
          'nt' => 'text/plain',
          'ttl' => 'text/turtle',
          'png' => 'image/png',
          'jpg' => 'image/jpeg',
          'gif' => 'image/gif',
          'bmp' => 'image/bmp',
          'pdf' => 'application/pdf',
          'zip' => 'application/zip',
          'gz' => 'application/gzip',
          'svg' => 'image/svg+xml',
        ];

        $allMimeTypes = array_merge($contentTypes, $this->getUserDefinedMimeTypes());

        return $allMimeTypes[$extension] ?? '';
    }

    /**
     * Add new/override existing mime types defined by user.
     *
     * @return array of user MIME definitions.
     */
    private function getUserDefinedMimeTypes(): array
    {
        $mimeTypes = [];
        $userTypes = $this->configuration->getConfigValue('conf', 'static')['mimetypes'];

        if (!$userTypes) {
            return $mimeTypes;
        }

        foreach ($userTypes as $extension => $mime) {
            $mimeTypes[$extension] = $mime;
        }

        return $mimeTypes;
    }

    /**
     * Validate that resource directory is valid and safe to use.
     *
     * @param string $file
     * @return void
     */
    private function validateDirectory(string $file): void
    {
        $componentsPath = $this->configuration->getConfigValue('conf', 'static')['directory'];

        $staticDirectory = realpath($componentsPath);
        $imgDirectory = realpath($componentsPath . 'img');
        $resourcePath = realpath($componentsPath . $file);

        if (strpos($resourcePath, $staticDirectory) !== 0 && strpos($resourcePath, $imgDirectory) !== 0) {
            HTTPStatus::send404($file);
        }
    }

}
