<?php declare(strict_types=1);

namespace uib\ub\loadspeakr;

use Exception;
use UnexpectedValueException;

final class Configuration
{
    private array $configs;

    /**
     * @throws Exception
     */
    public function __construct(array $conf)
    {
        if (count($conf) === 0) {
            throw new Exception(
              'Empty configuration. Giving up.'
            );
        }

        $this->configs['conf'] = $conf;
    }

    public function getConfigValue(string $type, $key)
    {
        if (!$key) {
            throw new UnexpectedValueException(
              'Configuration key missing : ' . $key
            );
        }
        if (!isset($this->configs[$type][$key])) {
            throw new UnexpectedValueException(
              'Missing config: ' . $type . ' of key: ' . $key
            );
        }

        return $this->configs[$type][$key];
    }

    public function getConfigValues($type): array
    {
        if (!isset($this->configs[$type])) {
            throw new UnexpectedValueException(
              'Unknown configuration type: ' . $type
            );
        }

        return $this->configs[$type];
    }

    public function debugMode(): bool
    {
        return $this->configs['conf']['debug'];
    }

    public function add(string $name, array $config): Configuration
    {
        $copy = clone $this;
        $copy->configs[$name] = $config;

        return $copy;
    }

}
