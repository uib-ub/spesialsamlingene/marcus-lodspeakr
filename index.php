<?php declare(strict_types=1);

use uib\ub\loadspeakr\Endpoint;
use uib\ub\loadspeakr\Exporter;
use uib\ub\loadspeakr\HTTPStatus;
use uib\ub\loadspeakr\Importer;
use uib\ub\loadspeakr\LoadModules;
use uib\ub\loadspeakr\Logging;
use uib\ub\loadspeakr\Utils;

const LOADSPEAKR_ROOT = __DIR__;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/common.inc.php';

if (file_exists(__DIR__ . '/settings.inc.php')) {
    require_once __DIR__ . '/settings.inc.php';
}

$lodspk['maxResults'] = 1000;

global $conf;
$configuration = new uib\ub\loadspeakr\Configuration($conf);
$utils = new Utils($configuration);

if (isset($_GET['q']) && $_GET['q'] === 'import') {
    $imp = new Importer($configuration);
    $imp->run();
    exit(0);
}

// Check that application is installed.
if (!file_exists('settings.inc.php')) {
    echo 'Need to configure lodspeakr first. Please run "install.sh". Alternatively, you can <a href="' . $_SERVER['HTTP_HOST'] . '/import">import an existing application</a>';
    exit(0);
}

$conf['logfile'] = null;
if ($conf['debug']) {
    if (isset($_GET['q']) && $_GET['q'] === 'logs') {
        Logging::init();
        exit(0);
    }

    $conf['logfile'] = Logging::createLogFile($_GET['q']);
}

$results = array();
$firstResults = array();
$endpoints['local'] = new Endpoint($conf['endpoint']['local'], $conf['endpointParams']['config']);
$acceptContentType = Utils::getBestContentType($_SERVER['HTTP_ACCEPT'], $conf['http_accept']);
$extension = Utils::getExtension($acceptContentType, $conf['http_accept']);
$uri = $conf['basedir'] . $_GET['q'];

// Check that content type is supported by LODSPeaKr.
if ($acceptContentType === null) {
    HTTPStatus::send406($uri);
}

if ($conf['export'] && $_GET['q'] === 'export') {
    $exp = new Exporter();
    header('Content-Type: text/plain');
    $exp->run();
    exit(0);
}

// Redirect to root URL if necessary.
$localUri = $uri;
if ($uri === $conf['basedir']) {
    header('Location: ' . $conf['root']);
    exit(0);
}


// Configure external URIs if necessary.
$localUri = $conf['basedir'] . $_GET['q'];
$uri = Utils::getMirroredUri($localUri);

/**
 * Update running configuration before launching Loadspekr modules.
 */
global $lodspk;
$boostrapConfig = [
    'localUri' => $localUri,
    'uri' => $uri,
    'acceptContentType' => $acceptContentType,
    'endpoints' => $endpoints,
    'extension' => $extension,
    'lodspk' => $lodspk,
    'firstResults' => $firstResults,
    'results' => $results,
];
$modulesConfig = $configuration->add('bootstrap', $boostrapConfig);

/**
 * Load Loadspeakr modules.
 */
foreach ($conf['modules']['available'] as $loadspkrModule) {
    $loader = new LoadModules($modulesConfig, new Utils($modulesConfig));
    $module = $loader->loadModule($loadspkrModule);
    $matching = $module->match($uri);

    if ($matching) {
        $module->execute($matching);

        if ($conf['logfile'] !== null) {
            fwrite($conf['logfile'], "]}");
            fclose($conf['logfile']);
        }

        /**
         * Only run first Loadspeakr modules that matches.
         */
        exit(0);
    }
}

HTTPStatus::send404($uri);
