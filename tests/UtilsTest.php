<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use uib\ub\loadspeakr\Utils;

final class UtilsTest extends TestCase
{
    private array $http_accept;

  public function setUp(): void
  {
      parent::setUp();
      $this->http_accept = [
        'html' => ['text/html'],
        'rdf' => ['application/rdf+xml'],
        'ttl' => [
          'text/n3',
          'application/x-turtle',
          'application/turtle',
          'text/turtle',
          'application/rdf+turtle',
        ],
        'json' => [
          'application/json',
          'application/x-javascript',
          'text/javascript',
          'text/x-javascript',
          'text/x-json',
        ],
        'nt' => ['text/plain'],
      ];
  }

    /**
     * Test Loadspeakr content negotiation.
     *
     * @dataProvider requestProvider
     * @covers \uib\ub\loadspeakr\Utils::getBestContentType
     */
  public function testGetBestContentType($expected, $requestHeader): void
  {
      self::assertSame($expected, Utils::getBestContentType($requestHeader, $this->http_accept));
  }

    /**
     * Test Loadspeakr extension negotiation.
     *
     * @dataProvider requestGetExtensionProvider
     * @covers \uib\ub\loadspeakr\Utils::getExtension
     */
    public function testGetExtension($expected, $mediaType): void
    {
        self::assertEquals($expected, Utils::getExtension($mediaType, $this->http_accept));
    }


    /**
     * @see testGetBestContentType
     */
  public function requestProvider(): array
  {

      return [
        'null header' => [
            'expected' => 'text/html',
            'media-type' => 'text/html',
        ],
        'Empty header' => [
            'expected' => 'text/html',
            'media-type' => 'text/html',
        ],
        'Empty string' => [
            'expected' => 'text/html',
            'media-type' => 'text/html',
        ],
        'Illegal string' => [
          'expected' => 'text/html',
          'media-type' => 'foo, bar',
        ],
        'html request' => [
          'expected' => 'text/html',
          'media-type' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        ],
        'CSS request' => [
          'expected' => 'text/html',
          'media-type' => 'text/css,*/*;q=0.1',
        ],
        'JSON request' => [
          'expected' => 'application/json',
          'media-type' => 'json/txt,application/json,*/*;q=0.1',
        ],
        'Illegal JSON' => [
          'expected' => 'text/html',
          'media-type' => 'json/born, application/jsons, */* ;q=0.1',
        ],
      ];
  }

    /**
     * @see testGetExtension
     */
  public function requestGetExtensionProvider(): array
  {
      return [
        'Text html' => [
            'expected' => 'html',
            'media-type' => 'text/html',
        ],
        'Unknown media type' => [
            'expected' => 'html',
            'media-type' => 'foo/bar',
        ],
        'Empty string' => [
            'expected' => 'html',
            'media-type' => '',
        ],
        'Plain text' => [
          'expected' => 'nt',
          'media-type' => 'text/plain',
        ],
        'CSS request' => [
          'expected' => 'json',
          'media-type' => 'application/json',
        ],
        'JSON request' => [
          'expected' => 'json',
          'media-type' => 'application/json',
        ],
        'Illegal JSON' => [
          'expected' => 'html',
          'media-type' => 'application/jsons',
        ],
        'Image jpeg' => [
          'expected' => 'html',
          'media-type' => 'image/jpeg',
        ],
      ];
  }

}
