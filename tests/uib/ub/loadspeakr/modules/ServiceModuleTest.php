<?php

namespace uib\ub\loadspeakr\modules;

use PHPUnit\Framework\TestCase;
use uib\ub\loadspeakr\Configuration;
use uib\ub\loadspeakr\Endpoint;
use uib\ub\loadspeakr\Utils;

require_once __DIR__ . '/../../../../../common.inc.php';

class ServiceModuleTest extends TestCase
{
    private ServiceModule $service;

    public function setUp(): void
    {
        parent::setUp();
        global $conf;
        $conf['basedir'] = 'https://example.com/';
        $conf['root'] = 'home';
        $lodspk = [
          'view' => '',
          'model' => '',
        ];
        $lodspk["title"] = 'Test';

        $boostrapConfig = [
          'localUri' => '',
          'uri' => 'https://example.combar.jpg',
          'acceptContentType' => 'text/html',
          'endpoints' => [],
          'extension' => [],
          'lodspk' => $lodspk,
        ];

        $boostrapConfig['endpoints']['local'] = new Endpoint('https://sparql.ub.uib.no/sparql/query', []);
        $configuration = new Configuration($conf);
        $modulesConfig = $configuration->add('bootstrap', $boostrapConfig);
        $this->service = new ServiceModule($modulesConfig, new Utils($modulesConfig));
    }

    /**
     * @covers \uib\ub\loadspeakr\modules\ServiceModule::match
     */
    public function testMatchGuards(): void
    {
        self::assertFalse($this->service->match('bar'), 'Failed matching');
    }

}
