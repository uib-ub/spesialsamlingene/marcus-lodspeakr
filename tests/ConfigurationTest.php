<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use uib\ub\loadspeakr\Configuration;

final class ConfigurationTest extends TestCase
{
    public Configuration $configuration;

    public function setUp(): void
    {
        $conf = [
            'foo' => 'bar',
            'debug' => true,
            'multiple' => ['one', 'two']
        ];

        $this->configuration = new Configuration($conf);
    }


    /**
     * @covers \uib\ub\loadspeakr\Configuration::getConfigValue
     */
    public function testGetConfigValue(): void
    {
        self::assertSame('bar', $this->configuration->getConfigValue('conf', 'foo'));
        self::assertCount(2, $this->configuration->getConfigValue('conf', 'multiple'),
        'Unable to get settings storing multiple values.'
        );
    }

    /**
     * @covers \uib\ub\loadspeakr\Configuration::getConfigValues
     */
    public function testGetConfigValues(): void
    {
        self::assertCount(3, $this->configuration->getConfigValues('conf'),
          'Unable to get settings type.'
        );
    }

    /**
     * @covers \uib\ub\loadspeakr\Configuration::getConfigValue
     */
    public function testException(): void
    {
        $this->expectException(UnexpectedValueException::class);
        $this->configuration->getConfigValue('Fail', 'error');
    }

    /**
     * @covers \uib\ub\loadspeakr\Configuration::debugMode
     */
    public function testDebugMode(): void
    {
        self::assertTrue($this->configuration->debugMode());
    }
}
